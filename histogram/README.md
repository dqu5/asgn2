<h1>PART B - Histogram<h1>
Dan Qu 
dqu5@ucsc.edu

This project takes continuous input of positive integers and displays a resulting histogram to the user. The histogram is limited to 16 "bins" and is adaptive to the range of the inputs, with each of the "bins" showing the corresponding range of integers displayed within the "bin". The integers are represented by asterisk (*) and stack with duplicates.


*histogram.c* contains the source code for the program
*Makefile* contains dependencies and compiler
*testing.sh* runs a script to test for functionality, with edge case tests, with the resulting output in *testing.out*

For compiling, Makefile is available in commandline with command "make", or directly compile with gcc with commandline prompt "gcc histogram.c -o histogram.o"

To run, simply enter "./histogram" in commandline while within directory "histogram"
